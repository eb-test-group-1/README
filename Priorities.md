## Quarterly Priorities
This content is meant to communicate how I intend to allocate my time. I review it weekly, but it should largely remain consistent on the time-scales of quarters.

| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Team Development | CDF Reviews, Career Development, GitLab Values Coaching | 40% |
| Sensing Mechanisms | Customer interviews, analyst inquiries, competitive review | 15% |
| Cross Section Product Experience | Thnk Bigs, Walk-throughs, learning goals, direction content review | 15% |
| Product Management Leadership Priorities | North Star Metrics, Data and ROI focus | 10% |
| External Evangelism | Ops vision, analyst briefings, conference speaking | 10% |
| Core Team Engagement | UX, Development, Quality | 5% |
| Personal Growth / Leadership Opportunities | Representing GitLab externally, Representing Product internally | 5% |
| Hiring | Sourcing, interviewing, hiring | 0% |

## Weekly Priorities
This content is meant to communicate my priorities on a weekly basis. They should typically be reflected as items in the `Doing` of [my personal issue board](https://gitlab.com/groups/gitlab-com/-/boards/1353560?assignee_username=kencjohnston). It will be updated weekly. As I'm always striving to learn I'll also add a `Learning Goal` each week.

### Legend
These designations are added to the previous week's priority list when adding the current week's priority.
* **Y** - Completed
* **N** - Not completed

## 2020-05-11
1. Come to a decision on timeline for rollout of this [Efficiency initiative](https://gitlab.com/gitlab-org/gitlab/-/issues/210457)
1. Finalize [Ops Section North Star Metric Review Follow Up](https://gitlab.com/gitlab-com/Product/-/issues/1049)
1. [Kickoff Prep](https://gitlab.com/gitlab-com/Product/-/issues/1156)
1. [Prepare a Microservices Sample Project](https://gitlab.com/gitlab-com/Product/-/issues/839) in order to [Document and Record a New Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/951) next week
1. Follow Up and Goal Setting from [NSM Review](https://gitlab.com/gitlab-com/Product/-/issues/1049)
1. Learning Goal - [Multi-Project Pipelines](https://gitlab.com/groups/gitlab-org/-/epics/3156#note_340245113) 

## 2020-05-04
1. Y - Finalize [Ops Section GC Prep](https://gitlab.com/gitlab-com/Product/-/issues/1108)
1. N - [Prepare a Microservices Sample Project](https://gitlab.com/gitlab-com/Product/-/issues/839) in order to [Document and Record a New Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/951) next week
1. Y - [Ops Section North Star Metric Review Follow Up](https://gitlab.com/gitlab-com/Product/-/issues/1049)
1. Y - [Ensure Regular CDF Reviews for Ops Section PMs](https://gitlab.com/gitlab-com/Product/-/issues/1136)
1. Y - [Transition Stable Counterparts for Configure and Monitor](https://gitlab.com/gitlab-com/Product/-/issues/1135)
1. Y - Learning Goal - [Advanced Feature Flag Usage](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html) 

## 2020-04-27
Note - short week for me as I'm taking an PTO day on Thursday and Family and Friends First day on Friday.
1. Y - [Cover for Thao who is on PTO](https://gitlab.com/gitlab-com/Product/-/issues/1048)
1. Y - Participate in the [Configure Design Sprint](https://gitlab.com/groups/gitlab-org/configure/-/epics/3)
1. Y - [Prep for new combined Ops Section GC](https://gitlab.com/gitlab-com/Product/-/issues/1108)
1. N - [Prepare a Microservices Sample Project](https://gitlab.com/gitlab-com/Product/-/issues/839) in order to [Document and Record a New Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/951) next week
1. Y - Learning Goal - [Parent Child Pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html)

## 2020-04-20
1. Y - [Cover for Thao who is on PTO](https://gitlab.com/gitlab-com/Product/-/issues/1048)
1. Y - Attend the Virtual CAB on 2020-04-21
1. Y - Attend Virtual Contribute on 2020-04-23
1. Y- [Communicate how our vision for a "Jenkins Importer" has undergone some iteration](https://gitlab.com/gitlab-com/Product/-/issues/1071) 
1. N - [Prepare a Microservices Sample Project](https://gitlab.com/gitlab-com/Product/-/issues/839) in order to [Document and Record a New Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/951) next week
1. N - Learning Goal - [Parent Child Pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html)

## 2020-04-13
1. Y - Prepare for [North Star Metric Review](https://gitlab.com/gitlab-com/Product/-/issues/880) - Ops on 2020-04-14, follow up from Review
1. Y - [Review all Group Planning issues](https://gitlab.com/gitlab-com/Product/-/issues/989) to continue getting up to speed
1. N - [Prepare a Microservices Sample Project](https://gitlab.com/gitlab-com/Product/-/issues/839) in order to [Document and Record a New Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/951) next week
1. N - [CLeanup Ops Section Themes](https://gitlab.com/gitlab-com/Product/-/issues/994) as part of feedback from Direction review
1. Y - Learning Goal - [Multi-Project Pipelines ](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html)

## 2020-04-06
1. Y - Respond to request for [Ops Section Direction review from PM team](https://gitlab.com/gitlab-com/Product/-/issues/986)
1. N - [Review all Group Planning issues](https://gitlab.com/gitlab-com/Product/-/issues/989) to continue getting up to speed
1. N - [Document and Record a New Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/951)
1. N - Prepare for [North Star Metric Review](https://gitlab.com/gitlab-com/Product/-/issues/880) - Ops on 2020-04-14

## 2020-03-30
1. Y - [Review to learn](https://gitlab.com/gitlab-com/Product/-/issues/947) - Review all Ops Section stage and category direction pages
1. Y - [Review all North Star Metric proposals for Product Groups](https://gitlab.com/gitlab-com/Product/-/issues/880)
1. Y - [Complete the combination of Ops and CI/CD section direction pages](https://gitlab.com/gitlab-com/Product/-/issues/912)
1. N - [Document and Record a New Ops Section Walk Through](https://gitlab.com/gitlab-com/Product/-/issues/951)

## 2020-03-23
1. Y - Prepare and get up to speed for [Jason's parental leave](https://gitlab.com/gitlab-com/Product/-/issues/911)
1. Y - Make significant steps to [combining the Ops and CI/CD section direction pages](https://gitlab.com/gitlab-com/Product/-/issues/912)
1. Y - Checkin on team members as they find new normal
1. Y - Ensure we're advancing the [Adoption of North Star Metrics in Product Groups](https://gitlab.com/gitlab-com/Product/-/issues/880)