## Kenny's README
My name is Kenny Johnston and I'm the Senior Director of Product, Ops at GitLab.
* [LinkedIn](https://www.linkedin.com/in/kencjohnston/)
* [Twitter](https://twitter.com/kencjohnston)
* [GitLab](https://gitlab.com/kencjohnston)
* [GitLab Team Page](https://about.gitlab.com/company/team/#kencjohnston)
* [Personal Feedback Form](https://forms.gle/uQgn9acSgFesBKWe7)

If we are working together, or even if we aren't, here are some things it might be helpful to know about me including those items marked with 👣 (the GitLab emojii for [iteration](https://about.gitlab.com/handbook/values/#iteration)) that I'm working on improving:

### General 
* I've worked in and around open-source for most of my technical career, I'm a firm believer in the [benefits of open code and open-society](https://cyber.harvard.edu/works/lessig/opensocd1.pdf) and the [importance of community](https://thenewstack.io/power-community-open-source/) in open-source projects.
* I had a previous career in political campaigns. While I [don't bring my political views to work](https://about.gitlab.com/handbook/values/#dont-bring-religion-or-politics-to-work), I do bring my belief that everyone desires to be a [valued member of a winning team on an inspiring mission](https://blog.rackspace.com/whats-core-rackspace-core-values). 
* My strengths tend towards the strategic, I work hard to focus on [getting things done](https://en.wikipedia.org/wiki/Getting_Things_Done) while continuously [improving my strategic thinking skills](https://hbr.org/2016/12/4-ways-to-improve-your-strategic-thinking-skills). 
* For a someone with strategic strengths I'm surprisingly task motivated - I enjoy breaking down tasks and checking off lists. I look at my [GitLab Activity](https://gitlab.com/kencjohnston) a bit too often.
* I'm demotivated by work without accomplishment. Meetings with conversation that doesn't end in something concrete frustrate me. Knowing I have a day full of non-action oriented meetings makes me want to hit the snooze button. 
* 👣 I often get bogged down in a search for efficiency, especially with new tasks. Feel free to call me out on it.
* I believe in [working smarter not harder](https://www.inc.com/john-rampton/work-smarter-not-harder-10-ways-to-be-more-effective-at-work.html), in an effort to [scale myself](https://firstround.com/review/our-6-must-reads-for-scaling-yourself-as-a-leader/). 
* I'm even-keeled. That can lead others to read into tone that wasn't there. I try to [prevent that from happening](https://www.fastcompany.com/3054178/5-ways-to-avoid-a-massive-email-misunderstanding).
* 👣 I've been known to come off as direct, and antagonistic in written communication. I can assure you I never intend to be antagonistic. I'm working on [conveying appropriate emotion](https://about.gitlab.com/company/culture/all-remote/informal-communication/#using-emojis-to-convey-emotion) in my direct, written communication. 
* I take my work seriously, but not myself. I think we could all use a little bit [more FUN in our work](https://www.entrepreneur.com/article/288223).
* I enjoy receiving public praise, but private praise fuels me. 
* I'm never the smartest person in the room, if I share opinions consider them [Minimally Viable](https://about.gitlab.com/handbook/values/#minimum-viable-change-mvc)
* I prefer to discuss things directly, both with my teams and my manager. I'd rather know someone's direct intention than to be coached around an issue socratically.
* I believe in [playing to your strengths](https://hbr.org/2005/01/how-to-play-to-your-strengths), and building complete teams based on coordinated strengths. 
* 👣 I aspire to use [inclusive language](https://docs.google.com/presentation/d/186RK9QqOYxF8BmVS15AOKvwFpt4WglKKDR7cUCeDGkE/edit?usp=sharing), eliminate [co-optation](https://www.youtube.com/watch?v=zNCrMEOqHpc) and create an inclusive environment. Please call out when I make mistakes or fail. I will ALWAYS thank you for it.
* My dog [Petri](https://about.gitlab.com/company/team-pets/#87-petri) shows up frequently in Zoom calls. She's big, and can startle the un-initiated. Sometimes I'll leave my camera's view to let her outside. 
* Outside of work I love to cook, play soccer, build puzzles and travel. 

### Product
* I LOVE the function of Product Management. In my first tech-job after meeting a Product Manager I immediately thought - "That is what I want to do!"
* I'm a strong believer that while product managers are typically hyper-efficient individuals, they are one of the few individuals driving a company's effectiveness.
* I really enjoy coaching, mentoring and advising Product Managers. Do not hestitate to reach out to me.
* 👣 I'm working on striking the right balance between providing more direct input into Product Direction and empowering autonomy in teams I support. Please do not hesitate to call me out if I'm being overly directive.

### Management
* I aspire to be a servant leader, but I've got a long-way to go. Please provide me feedback to help me get there.
* I believe as a leader you get back what you give out. I once heard a talk that hypothesized that [Conway's Law](https://en.wikipedia.org/wiki/Conway%27s_law) tells us that if we want lovable results OUT of our teams, we have to have put love IN to them.
* I want each of my team members to know they are a valuable contributor to a winning team and give them an inspiring mission.
* I believe my role as a manager is to run straight at murkey or persistent problems. Feel free to point me in their direction. 
* I was changed by the book [Drive](https://www.amazon.com/Drive-Surprising-Truth-About-Motivates/dp/1594484805/r), it helped me understand motivation in a new way. I always want a [team of missionaries, not mercenaries](https://svpg.com/missionaries-vs-mercenaries/). 
* 👣 I'm working on being a better delegater, feel free to call me out if I'm leaving delegation ambigous or not delegating enough.
* I tend to be direct, and I HATE micro-managing.
* I want to be the type of leader I'd like to have - Loving, Trusting, Fair and Consistent.

### Logistics
* I block off my calendar for family time/dinner/bed-time from 1:00PM - 7:50PM CT.
* My day typically starts early with a block of dedicated focus/work time from 6:00AM - 8:50AM CT.
* When you need to reach me, here are my communication priorities and preference:
    * GitLab issues - Direct mention me on issues, I prefer interacting with issues to all other collaboration mechanisms. I aggressively follow up on TODOs. 
    * Slack - If I'm working, I'm on slack. For small group discussions, never hesitate to create a dedicated public chat room. Please avoid DMs unless they really need to be private.
    * Email - I check email only periodically throughout the day. I have no alerts setup to inform me of newly arriving email. 
    * SMS - I have my phone by my side during regular work hours to review texts or receive calls. Outside of work hours I'm hit or miss on SMS.
    * Phone - If I'm in a meeting I will let my phone go to voicemail. I only return calls that leave voicemails. If it is urgent, call my phone twice back-to-back and I will step out of what I'm doing to answer.
* I avoid email for the following activities (thanks GitLab!):
    * Long dialog requiring discussion - better to create an issue, start a conversation on slack or setup a meeting
    * Requests for review - better to create an issue, or start a slack converation and ping the required reviewers
* As mentioned above, GitLab issues are my preferred collaboration method. As a result, I'm an exclusive user of GitLab TODOs. Please @ mention me when responding to issues, otherwise I won't get the notification.
* I'm an avid leaver of slack channels. I do this to reduce the noise that can come with Slack. I'm only members of channels that I want to be notified of ANY new content. If I'm not in a channel don't hestitate to invite me, and don't be offended when I leave after responding. If you are interested - here's how I've configued my Slack Settings to further avoid noise.
    * I only show "Unreads Only"
    * I sort via "Priority"
    * I use the "Compact" Message Theme
    * I use "Just display names"
    * I do NOT require a "Prompt to confirm" when everything is read
* I consider Slack to be asynchronous. If there is a timeline for a request you might have please specificy it. For example - "Hey, can you review the Kickoff list prior to the kickoff meeting tomorrow?"
* I'm a big believer in inbox zero and I process my email, slack and GitLab TODO inbox to zero multiple times a day to keep my focus on my [personal GitLab issue board](https://gitlab.com/groups/gitlab-com/-/boards/1353560?assignee_username=kencjohnston).
* I use ToDoist for short-term tasks and reminders that aren't appropriate for GitLab issues.
* 👣 I will add FYI or FYA/R (and avoid the non-specific CC) to responses to ensure I'm setting expectations correctly.
* 👣 I'm still learning how to balance [multi-modal communication](https://about.gitlab.com/handbook/communication/#multimodal-communication) and that can sometimes result in more annoyance then effectiveness. Please don't hesitate to call me out if you feel like I'm over-communicating.

### Teams I Support
I [support the Product Management team](https://about.gitlab.com/company/team/org-chart/) responsible for the Ops Section stages at GitLab. 
* One easy way to keep me abreast of your priorities is to send a note in the #product Slack channel once a week and @ mention me. 
* Follow the [GitLab handbook when communicating time-off](https://about.gitlab.com/handbook/paid-time-off/#communicating-your-time-off). No other notification or approval is required.
* I will never take offense to skip-leveling, if you have a concern [you can and should take it to whoever you think will be effective in alleviating it](https://about.gitlab.com/handbook/leadership/#communication-should-be-direct-not-hierarchical).
* Share what you learn, whether that is relevant articles, conference notes or TED talks.
* When sharing info with me, my preference is to consume YOUR expertise. Rather than sharing an Analyst Report relevant for your categories, share an MR where you reference it and highlight how it impacts your product scope.

### Personality Tests
* [Strengths Finder](https://www.gallup.com/cliftonstrengths/en/252137/home.aspx?utm_source=google&utm_medium=cpc&utm_campaign=Strengths_ECommerce_Brand_Search_US&utm_content=strengths%20finder&gclid=CjwKCAjwguzzBRBiEiwAgU0FT6jwCih5cTNJnta-h6m3YsJ5mAQG4dRikPdeCp79Wjp1mT0bGamTpRoCIM4QAvD_BwE) - Futuristic, Learner, Strategic, Ideation, Intellection (Note these are ALL in the Strategic Thinking quadrant)
* [Myers-Briggs](https://en.wikipedia.org/wiki/Myers%E2%80%93Briggs_Type_Indicator) - [INFP](https://www.16personalities.com/infp-personality) - Borderline between I&E, strong N, border between T&F, extreme P

### Adaptable
I never shy from feedback, constructive criticism or suggested improvements. It happens so rarely in the corporate world, I will ALWAYS thank you for it. Please consider this document in that context. I'm accepting [issue submissions](https://gitlab.com/kencjohnston/README/issues) and [merge request](https://gitlab.com/kencjohnston/README/merge_requests).

You can also provide me direct (anonymous) feedback by filling out my [ongoing feedback form](https://forms.gle/uQgn9acSgFesBKWe7). 

I've marked the items I'm working on (mostly derived from direct feedback) with an iteration 👣. THANK YOU to all those who've helped me grow.